//
//  TestViewCell.swift
//  TableTest
//
//  Created by User on 8/28/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class TestViewCell: UITableViewCell {
    
    @IBOutlet weak var mainLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
