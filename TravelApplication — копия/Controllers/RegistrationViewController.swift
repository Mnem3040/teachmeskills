//
//  RegistrationViewController.swift
//  TravelApplication
//
//  Created by mnem on 10/13/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit
import Firebase

class RegistrationViewController: UIViewController {

    @IBOutlet weak var registrationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registrationButton.layer.cornerRadius = 4
        // Do any additional setup after loading the view.
    }
    

    @IBAction func registrationClicked(_ sender: Any) {
        let email = "test1@gmail.com"
        let password = "123456"
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let user = result?.user {
                print("User was created: \(user.email)")
            } else if let error = error {
                print("Create error: \(error)")
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
