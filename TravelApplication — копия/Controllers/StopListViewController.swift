//
//  StopListViewController.swift
//  TravelApplication
//
//  Created by User on 9/6/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class StopListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CreateStopViewControllerDelegate {
    
    func didCreate(stop: Stop) {
        travel?.stops.append(stop)
//        nameStopLabel.text = stop.name
//        discriptionLabel.text = stop.description
//        spentMoneyLabel.text = String(stop.spentMoney)
        stopTableView.reloadData()
        
    }
    
  
    //MARK: -Outlets
    
    @IBOutlet weak var spentMoneyLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var nameStopLabel: UILabel!
    @IBOutlet weak var stopTableView: UITableView!
    @IBOutlet weak var addBarButton: UIBarButtonItem!
    //MARK: -Properties
    
    var travel: Travel?
    
    //MARK: -LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stopTableView.delegate = self
        stopTableView.dataSource = self
        addBarButton.tintColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)

    }
    //MARK: - TableView

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travel?.stops.count ?? 0
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 138
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopCell", for: indexPath) as! StopCell
        cell.nameLebel.text = travel?.stops[indexPath.row].name
        cell.descriptionLabel.text = travel?.stops[indexPath.row].description
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let createVC = storyboard.instantiateViewController(identifier: "CreateStopViewController") as! CreateStopViewController
        createVC.delegate = self
        createVC.travelId = travel?.id ?? ""
        createVC.stop = travel?.stops[indexPath.row]
        navigationController?.pushViewController(createVC, animated: true)
    }
    //MARK: - Actions
    
    @IBAction func addStop(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addVC = storyboard.instantiateViewController(identifier: "CreateStopViewController") as! CreateStopViewController
        addVC.delegate = self
        addVC.travelId = travel?.id ?? ""
        navigationController?.pushViewController(addVC, animated: true)

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

