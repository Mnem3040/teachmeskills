//
//  UserListViewController.swift
//  NewTestTable
//
//  Created by User on 8/29/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class UserListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
  //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
   //MARK: - Prooerties
    var users: [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        for i in 0...500 {
            let user = User()
            user.name = "name:\(i)"
            user.email = "email:\(i)"
            users.append(user)
            
        }

        // Do any additional setup after loading the view.
    }
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count // - колличество ячеек
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row % 2 == 0 {
            return 98
        } else {
            return 212
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell1", for: indexPath) as! UserCell
            print(indexPath.row)
            cell.nameLabel.text = users[indexPath.row].name
            cell.emailLabel.text = users[indexPath.row].email
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell2", for: indexPath) as! UserWithCarCell
            print(indexPath.row)
            cell.NameWithCarLabel.text = users[indexPath.row].name
            cell.EmailWithCarLabel.text = users[indexPath.row].email
            return cell
        }
        
       
    }

}
