//
//  FirstViewController.swift
//  dataTranferDeligate
//
//  Created by User on 9/7/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    //MARK: - Actions
    @IBAction func pushClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(identifier: "SecondViewController") as! SecondViewController
        secondViewController.firstScreen = self
        navigationController?.pushViewController(secondViewController, animated: true)
        
    }
    //MARK: - Functions
    
    func onSecondScreenDidClickedSegmentedControl(withColor: UIColor) {
        view.backgroundColor = withColor
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
