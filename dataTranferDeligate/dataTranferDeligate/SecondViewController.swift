//
//  SecondViewController.swift
//  dataTranferDeligate
//
//  Created by User on 9/7/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    //MARK: - Outlets
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    //MARK: - Properties
    var firstScreen: FirstViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - Action
    
    @IBAction func clickedDone(_ sender: Any) {
        if segmentControl.selectedSegmentIndex == 0 {
            firstScreen?.onSecondScreenDidClickedSegmentedControl(withColor: .red)
        } else if segmentControl.selectedSegmentIndex == 1 {
            firstScreen?.onSecondScreenDidClickedSegmentedControl(withColor: .green)
        } else if segmentControl.selectedSegmentIndex == 2 {
            firstScreen?.onSecondScreenDidClickedSegmentedControl(withColor: .blue)
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func segmentControlChange(_ sender: Any) {
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
