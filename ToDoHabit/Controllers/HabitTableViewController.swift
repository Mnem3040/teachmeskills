//
//  HabitTableViewController.swift
//  ToDoHabit
//
//  Created by User on 10/1/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class HabitTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
//MARK: - Propertie
    var habit: [Habit] = []
    
    
    
//MARK: - Outlet
    @IBOutlet weak var habitTableView: UITableView!



    //MARK: - LifeCycle
    override func viewDidLoad() {
            super.viewDidLoad()
        habitTableView.delegate = self
        habitTableView.dataSource = self
        }
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return habit.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HabitCell", for: indexPath) as! HabitCell
    //    cell.nameLabel.text = travels[indexPath.row].name
    //    cell.descriptionLabel.text = travels[indexPath.row].description
        return cell
      }
    
    
    
//MARK: - Action
    @IBAction func addHabitAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let createHabitViewController = storyboard.instantiateViewController(identifier: "CreateHabitViewController") as! CreateHabitViewController
        present(createHabitViewController, animated: true, completion: nil)
    }
    @IBAction func habitRightSwipeAction(_ sender: Any) {
//        _ = UIStoryboard(name: "Main", bundle: nil)
//        let todoViewController = storyboard.instantiateViewController(identifier: "TodoTableViewController") as! TodoTableViewController
            navigationController?.popViewController(animated: true)
    }

    // MARK: - Table view data source
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 200
//    }
//    func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 2
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}
