//
//  CreateTodoViewController.swift
//  ToDoHabit
//
//  Created by mnem on 10/4/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class CreateTodoViewController: UIViewController {
//MARK: - Outlet
    @IBOutlet weak var enterTodoTextField: UITextField!
    @IBOutlet weak var dataPicker: UIDatePicker!
    @IBOutlet weak var remainderPicker: UIDatePicker!
    @IBOutlet weak var importantButtonOutlet: UIButton!
    
//MARK: - Propertie
//    let dataPicker = UIDatePicker()
    
    
    
//MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
    }
    
    
    

    
    //MARK: - Action
    @IBAction func importantButtonAction(_ sender: Any) {
    }
    
}
