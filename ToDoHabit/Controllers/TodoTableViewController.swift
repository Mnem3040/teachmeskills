//
//  TodoTableViewController.swift
//  ToDoHabit
//
//  Created by User on 10/1/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class TodoTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        todo.count
    }
//MARK: - Outlets
    @IBOutlet weak var todoTableView: UITableView!
    
    
 
    
//MARK: - Properties
    var todo: [ToDo] = []
    
    
    
//MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        todoTableView.delegate = self
        todoTableView.dataSource = self

    }

    
    
// MARK: - Table view data source
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath) as! TodoCell
        return cell
    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 2
//    }
    
    
    
//MARK: - Action
    @IBAction func addTodoAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let createTodoViewController = storyboard.instantiateViewController(identifier: "CreateTodoViewController") as! CreateTodoViewController
        present(createTodoViewController, animated: true, completion: nil)

    }
    @IBAction func todoRightSwipeAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let habitViewController = storyboard.instantiateViewController(identifier: "HabitTableViewController") as! HabitTableViewController
        navigationController?.pushViewController(habitViewController, animated: true)
        
    }
    @IBAction func todoUpSwipeAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let createTodoViewController = storyboard.instantiateViewController(identifier: "CreateTodoViewController") as! CreateTodoViewController
        present(createTodoViewController, animated: true, completion: nil)

    }
    
    


}
