//
//  Todo.swift
//  ToDoHabit
//
//  Created by mnem on 10/6/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class Todo {
    var name: String?
    var time: String?

    var reminder: String?
    var importantTask: Bool?
    var repeatTask: Bool?
}
