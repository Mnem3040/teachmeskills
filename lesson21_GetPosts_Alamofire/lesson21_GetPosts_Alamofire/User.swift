//
//  User.swift
//  lesson21_GetPosts_Alamofire
//
//  Created by User on 10/1/20.
//  Copyright © 2020 mnem. All rights reserved.
//


import UIKit

class User {
    var id: Int?
    var name: String?
    var email: String?
    var username: String?
    var address: Address?
    var phone: String?
    var website: String?
    var company: Company?
}
