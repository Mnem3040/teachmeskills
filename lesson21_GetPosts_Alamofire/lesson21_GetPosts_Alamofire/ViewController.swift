//
//  ViewController.swift
//  lesson21_GetPosts_Alamofire
//
//  Created by User on 10/1/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var geoLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var bigLabel: UILabel!
    
    
    var users: [User] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        
        AF.request("https://jsonplaceholder.typicode.com/users").responseJSON { (response) in
            guard let value = response.value as? [[String: Any]] else {
                return
            }
            for userDict in value {
                let user = User()
                user.id = userDict["id"] as? Int
                user.name = userDict["name"] as? String
                user.email = userDict["email"] as? String
                user.username = userDict["username"] as? String
                user.phone = userDict["phone"] as? String
                user.website = userDict["website"] as? String
                if let companyJson = userDict["company"] as? [String: Any] {
                    let company = Company()
                    company.name = companyJson["name"] as? String
                    company.cathRhrase = companyJson["catchPhrase"] as? String
                    company.bs = companyJson["bs"] as? String
                    user.company = company
                }
                if let addressJson = userDict["address"] as? [String: Any] {
                    let address = Address()
                    address.city = addressJson["city"] as? String
                    address.street = addressJson["street"] as? String
                    address.suite = addressJson["suite"] as? String
                    address.zipcode = addressJson["zipcode"] as? String
                    user.address = address
                    if let geoJson = addressJson["geo"] as? [String: Any] {
                        let geo = Geo()
                        geo.latitude = geoJson["lat"] as? String
                        geo.longitude = geoJson["lng"] as? String
                        user.address?.geo = geo
                    }
                }
                self.users.append(user)
                
            }
            self.tableView.reloadData()
        }

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TestViewCell", for: indexPath) as! TestViewCell
        let user = users[indexPath.row]
        if let id = user.id {
            cell.idLabel.text = "id : " + "\(id)"
        }
        cell.nameLabel.text = "name : " + user.name!
        cell.mailLabel.text = "mail : " + user.email!
//        cell.phoneLabel.text = "phone : " + user.phone!
        cell.userNameLabel.text = "userName : " + user.username!
        cell.bigLabel.text = """
            address: street : \(user.address?.street ?? "")
                            suite : \(user.address?.suite ?? "")
                            city : \(user.address?.city ?? "")
                            zipcode : \(user.address?.zipcode ?? "")
                            geo : \(user.address?.geo?.latitude ?? "")   \(user.address?.geo?.longitude ?? "")

            phone : \(user.phone ?? "")

            website : \(user.website ?? "")

            company: name : \(user.company?.name ?? "")
                              cathPrase :
                                \(user.company?.cathRhrase ?? "")
                              bs : \(user.company?.bs ?? "")
            """
//////        id: \(user.id)
//////        name: \(user.name ?? "")
//////        e-mail: \(user.email ?? "")
//////        nickname: \(user.username ?? "")

//        if let latitude = user.address?.geo?.latitude, let longitude = user.address?.geo?.longitude {
//            cell.website.text = latitude + " : " + longitude
//        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }

}


