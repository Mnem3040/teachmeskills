//
//  TestViewCell.swift
//  lesson21_GetPosts_Alamofire
//
//  Created by User on 10/1/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class TestViewCell: UITableViewCell {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var website: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var bigLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        website.numberOfLines = 2
      
    }

}
