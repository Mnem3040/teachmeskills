//
//  Address.swift
//  lesson21_GetPosts_Alamofire
//
//  Created by User on 10/1/20.
//  Copyright © 2020 mnem. All rights reserved.
//


import UIKit
class Address {
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
    var geo: Geo?
}
