//
//  FirstViewController.swift
//  DataTransferLesson
//
//  Created by User on 8/31/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buttom: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if let secondViewController = segue.destination as? SecondViewController {
            if segue.identifier == "test1" {
                secondViewController.transferText = "XXX"
            } else {
                secondViewController.transferText = "YYY"
            }
        }
       
        
    }
    

}
