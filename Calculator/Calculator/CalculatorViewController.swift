//
//  CalculatorViewController.swift
//  Calculator
//
//  Created by User on 7/30/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

enum Operations {
    case none, plus, minus, devide, multiple
}

class CalculatorViewController: UIViewController {
    
    var currentResult: Double = 0
    var isNeedToClearResultLabel: Bool = false
    var operation: Operations = .none
    
    @IBOutlet weak var resultLabel: UILabel!
    
   
    @IBAction func isClearAll(_ sender: UIButton) {
        currentResult = 0
        resultLabel.text = "0"
    }
    
    @IBAction func digitClicked(_ sender: UIButton) {
        if isNeedToClearResultLabel == true {
            resultLabel.text = ""
            isNeedToClearResultLabel = false
         
        }
        if resultLabel.text == "0" {
           resultLabel.text = sender.currentTitle
        } else {
            resultLabel.text = resultLabel.text! +
            sender.currentTitle!
        }
        
    }
    
    @IBAction func operationClicked(_ sender: UIButton) {
        if sender.currentTitle == "+" {
            operation = .plus
        } else if sender.currentTitle == "-" {
            operation = .minus
        } else if sender.currentTitle == "*" {
            operation = .multiple
        } else if sender.currentTitle == "/" {
            operation = .devide
        }
        if let text = resultLabel.text, let doubleValue = Double(text) {
            currentResult = doubleValue
        }
        isNeedToClearResultLabel = true
    }
    
    
    @IBAction func resultClicked(_ sender: UIButton) {
        var result: Double = 0
        let value2 = Double(resultLabel.text!)!
        switch operation {
        case .plus:
            result = currentResult + value2
        case .minus:
            result = currentResult - value2
        case .devide:
            result = currentResult / value2
        case .multiple:
            result = currentResult * value2
        
        default:
            break
        }
        resultLabel.text = String(result)
        operation = .none
        isNeedToClearResultLabel = true
      
    }
}
