//
//  testViewController.swift
//  TravelApplication
//
//  Created by mnem on 10/22/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit
import FirebaseRemoteConfig

class TestViewController: UIViewController {

    @IBOutlet weak var testViewLabel: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.fetchAndActivate { (status, error) in
            DispatchQueue.main.async {
                self.showWelcomeScreen()
            }
        }
    }
    func showWelcomeScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let stopVC = storyboard.instantiateViewController(identifier: "WelcomeViewController") as! WelcomeViewController
        self.navigationController?.pushViewController(stopVC, animated: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
