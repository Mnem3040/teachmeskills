//
//  ViewController.swift
//  lesson21_URL-session
//
//  Created by User on 10/1/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func getClicked(_ sender: Any) {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/users") else {return}
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, response, error) in
            print("Data", data ?? "")
//            print("Response", response)
            print("Error", error ?? "")
            guard let data = data else {
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print("JSON:", json)
            } catch {
                print(error)
            }
        }
        task.resume()
    }
    @IBAction func postClicked(_ sender: Any) {
         guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts") else {return}
        let parameters = ["login": "alex", "password":"12345678"]
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            print("Data", data ?? "")
            //            print("Response", response)
            print("Error", error ?? "")
            guard let data = data else {
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print("JSON:", json)
            } catch {
                print(error)
            }
        }
        task.resume()
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

