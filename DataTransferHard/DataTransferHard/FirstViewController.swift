//
//  ViewController.swift
//  DataTransferHard
//
//  Created by User on 8/31/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func buttonClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(identifier: "SecondViewController") as! SecondViewController
        secondViewController.transferText = textField.text ?? ""
        navigationController?.pushViewController(secondViewController, animated: true)
    }
    

    
}
