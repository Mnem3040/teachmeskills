//
//  SecondViewController.swift
//  DataTransferHard
//
//  Created by User on 8/31/20.
//  Copyright © 2020 mnem. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    var transferText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = transferText
       
    }
    

    @IBAction func backClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
